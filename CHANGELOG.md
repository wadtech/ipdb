CHANGELOG
---------

v0.1.a.1
 - fixed asset view logic raising an exception when an asset has no IP address assigned to it.

v0.1.a
 - first release
