# Place all the behaviors and hooks related to the matching controller here.
# All this logic will automatically be available in application.js.
# You can use CoffeeScript in this file: http://jashkenas.github.com/coffee-script/
window.stripeTable = (table) ->
  console.log("redecorating Table")
  $("tr:even:not(:first)", table).removeClass($(this).attr('class')).addClass("even")
  $("tr:odd", table).removeClass($(this).attr('class')).addClass("odd")
  highlightUnapproved()

highlightUnapproved = ->
  $('.approval-cell').each ->
    console.log("Highlighting Unapproved Users")
    if($(this).is(":contains('Approve')"))
      $(this).parent().removeClass($(this).attr('class')).addClass('disabled')

jQuery ->
  stripeTable($('.users-table'))