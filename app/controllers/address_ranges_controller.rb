class AddressRangesController < ApplicationController

  before_filter :authenticate_user!
  
  def index
    @ranges = AddressRange.all
  end

  def show
    @range = AddressRange.find(params[:id])
  end

  def create
    @range = AddressRange.new(params[:address_range])

    respond_to do |format|
      if @range.save
        format.html { redirect_to @range, notice: 'IP Range was successfully created.' }
        format.json { render json: @range, status: :created, location: @range }
      else
        format.html { render action: "new" }
        format.json { render json: @range.errors, status: :unprocessable_entity }
      end
    end
  end

  def new
    @range = AddressRange.new
  end

  def edit
    @range = AddressRange.find(params[:id])
  end

  def update
    @range = AddressRange.find(params[:id])

    respond_to do |format|
      if @range.update_attributes(params[:address_range])
        format.html { redirect_to address_ranges_path, notice: 'Range was successfully updated.' }
        format.json { head :no_content }
      else
        format.html { render action: "edit" }
        format.json { render json: @range.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @range = AddressRange.find(params[:id])
    @range.destroy

    respond_to do |format|
      format.html { redirect_to address_ranges_url }
      format.json { head :no_content }
    end
  end
end
