class Admin::UsersController < ApplicationController
  before_filter :authenticate_user!

  def new
    if !current_user.admin?
      redirect_to root_url, :alert => "Access Denied."
    else
      @user = User.new
    end
  end

  def index
    if !current_user.admin?
      redirect_to root_url, :alert => "Access Denied."
    else
    @users = User.all
    end
  end

  def edit
    if !current_user.admin?
      redirect_to root_url, :alert => "Access Denied."
    else
    @user = User.find(params[:id])
    end

    respond_to do |format|
      format.html
    end
  end

  def toggle_admin
    @user = User.find(params[:id])
    @user.toggle!(:admin)

    respond_to do |format|
      format.js
    end
  end

  def toggle_approval
    @user = User.find(params[:id])
    @user.toggle!(:approved)
  
    respond_to do |format|
      format.js
    end
  end

  def create
    @user = User.new(params[:user])

    respond_to do |format|
      if @User.save
        format.html { redirect_to users_path, notice: 'User was successfully created.' }
        format.json { render json: @user, status: :created, location: @user }
      else
        format.html { render action: "new" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def update
    @user = User.find(params[:id])

    respond_to do |format|
      if @user.update_attributes(params[:user])
        format.html { redirect_to admin_users_url, notice: 'User was successfully updated.' }
        format.json { head :ok }
      else
        format.html { render action: "edit" }
        format.json { render json: @user.errors, status: :unprocessable_entity }
      end
    end
  end

  def destroy
    @user = User.find(params[:id])
    @user.destroy

    respond_to do |format|
      format.html { redirect_to users_url }
      format.json { head :ok }
    end
  end
end
