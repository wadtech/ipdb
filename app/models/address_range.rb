# == Schema Information
#
# Table name: address_ranges
#
#  id         :integer         not null, primary key
#  start_ip   :string(255)
#  end_ip     :string(255)
#  created_at :datetime        not null
#  updated_at :datetime        not null
#  ip_id      :integer
#

class AddressRange < ActiveRecord::Base
  has_many :ips, :foreign_key => :ip_id
  has_many :assets

  validates :start_ip, :end_ip,
      :presence => true, 
      :format => { :with => /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/ }


  def full_name
    "#{start_ip} - #{end_ip}"
  end
end
