# == Schema Information
#
# Table name: assets
#
#  id               :integer         not null, primary key
#  name             :string(255)
#  ip_id            :integer
#  address_range_id :integer
#  comments         :text
#  last_edit        :string(255)
#  created_at       :datetime        not null
#  updated_at       :datetime        not null
#

class Asset < ActiveRecord::Base
  belongs_to :ip

  validates :name, :last_edit, :presence => true

end
