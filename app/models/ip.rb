# == Schema Information
#
# Table name: ips
#
#  id               :integer         not null, primary key
#  ip_name          :string(255)
#  range_id         :integer
#  asset_id         :integer
#  created_at       :datetime        not null
#  updated_at       :datetime        not null
#  address_range_id :integer
#

class Ip < ActiveRecord::Base
  belongs_to :address_range, :foreign_key => :address_range_id
  has_one :asset

  validates :address_range, :ip_name, :presence => true
  validates :ip_name, :format => { :with => /\b(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\b/ }
end
