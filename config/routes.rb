Ipdb::Application.routes.draw do
 
  resources :assets

  root :to => "home#index"

  devise_for :users

  resources :address_ranges
  resources :ips
  
  namespace :admin do
    resources :users do 
      get 'toggle_approval', :on => :member
      get 'toggle_admin', :on => :member  
    end
  end
end
