class CreateTableAddressRanges < ActiveRecord::Migration
  def change
    create_table :address_ranges do |t|
      t.string :start_ip
      t.string :end_ip
 
      t.timestamps
    end
  end
end
