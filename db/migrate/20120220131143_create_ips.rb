class CreateIps < ActiveRecord::Migration
  def change
    create_table :ips do |t|
      t.string :ip_name
      t.integer :range_id
      t.integer :asset_id

      t.timestamps
    end
  end
end
