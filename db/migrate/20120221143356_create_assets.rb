class CreateAssets < ActiveRecord::Migration
  def change
    create_table :assets do |t|
      t.string :name
      t.integer :ip_id
      t.integer :address_range_id
      t.text :comments
      t.string :last_edit

      t.timestamps
    end
  end
end
