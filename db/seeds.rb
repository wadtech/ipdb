# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
puts 'SETTING UP DEFAULT USER LOGIN'
user = User.create! :name => 'Administrator', :email => 'admin@ipdb.com', :password => 'secret', :password_confirmation => 'secret', :admin => true, :approved => true
puts 'New user created: ' << user.name

puts 'SETTING UP SAMPLE APPROVED USER'
user = User.create! :name => 'Approved User', :email => 'approved@ipdb.com', :password => 'password', :password_confirmation => 'password', :admin => false, :approved => true
puts 'New user created: ' << user.name

puts 'SETTING UP DEFAULT UNAPPROVED USER'
user = User.create! :name => 'Unapproved User', :email => 'unapproved@ipdb.com', :password => 'password', :password_confirmation => 'password', :admin => false, :approved => false
puts 'New user created: ' << user.name